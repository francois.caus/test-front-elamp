import React from "react";
import EditableField from "./EditableField";

const APP = () => {
  return (
    <>
      <EditableField name="Name" value="" mode="read-only" />
    </>
  );
};

export default APP;
