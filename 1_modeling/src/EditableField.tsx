import React, { useState } from "react";

type Modes = "read-only" | "edit";

type EditableFieldProps = {
  name: string;
  value: string;
  mode: Modes;
};

type KeyboardEvent = {
  key: string;
};
const EditableField = (props: EditableFieldProps) => {
  const [mode, setMode] = useState<Modes>(props.mode);

  const handleOnChange = () => {
    setMode("edit");
  };
  const handleClick = () => {
    setMode("read-only");
  };
  const handleKeyDown = (event: KeyboardEvent) => {
    if (event.key === "Enter") {
      setMode("read-only");
    }
  };

  return (
    <form
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "start",
        width: "200px",
        margin: "0 auto ",
      }}
    >
      <label htmlFor="name">{props.name}</label>
      <input
        type="text"
        id="name"
        onChange={handleOnChange}
        onKeyDown={handleKeyDown}
      />
      {mode === "edit" && <button onClick={handleClick}>Valider</button>}
    </form>
  );
};

export default EditableField;
