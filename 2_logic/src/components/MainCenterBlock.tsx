import React, { useState } from "react";
import useApi from "./hooks/useApi";
import ListItem from "./ListItem";

const MainContentBlock: React.FC = () => {
  const { data, loaded } = useApi("../models/api.ts");
  //utiliser le context de itemContext pour gérer la selection d'un item
  return (
    <div>
      <ul>
        {loaded &&
          data?.map((item, index) => <ListItem name={item.data[index].name} />)}
      </ul>
    </div>
  );
};

export default MainContentBlock;
