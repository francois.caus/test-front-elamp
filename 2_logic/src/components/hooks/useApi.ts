import React, { useEffect, useState } from "react";

interface Data {
  id: string;
  name: string;
  details: Details;
}

interface Details {
  field1: string;
  field2: boolean;
  field3: string[];
}

interface DataFetch {
  count: number;
  data: Data[];
}

const useApi = (url: string): { data: DataFetch[] | null; loaded: boolean } => {
  const [data, setData] = useState<DataFetch[] | null>(null);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((res: DataFetch[]) => {
        setData(res);
        setLoaded(true);
      })
      .catch((err) => console.error("ERROR ", err));
  }, [url]);
  return { data, loaded };
};

export default useApi;
