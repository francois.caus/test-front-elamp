import React from "react";
import PropTypes from "prop-types";

type ListItemProps = {
  name: string;
};

const ListItem = (props: ListItemProps) => {
  return (
    <div>
      <p>{props.name}</p>
    </div>
  );
};

ListItem.propTypes = {
  name: PropTypes.string,
};

export default ListItem;
