import React from "react";
import MainCenterBlock from "./MainCenterBlock";
import RightDetailPane from "./RightDetailPane";

const MainPage: React.FC = () => {
  //utiliser le context blockContext pour gérer l'affichage du block de droite
  return (
    <>
      <MainCenterBlock />
      {/* afficher si isShow est true */}
      <RightDetailPane />
    </>
  );
};

export default MainPage;
