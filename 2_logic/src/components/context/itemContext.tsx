import React, {
  createContext,
  Dispatch,
  SetStateAction,
  useState,
} from "react";
import { useContext } from "react";

type TItemSelected = {
  name: string;
  setCurrent: Dispatch<SetStateAction<string>>;
};

export const Context = createContext({
  name: "",
} as TItemSelected);

export const Provider: React.FC = ({ children }) => {
  const [current, setCurrent] = useState<string>("");
  return (
    <Context.Provider
      value={{
        name: current,
        setCurrent,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useContextItem = () => useContext(Context);
