import React, {
  createContext,
  Dispatch,
  SetStateAction,
  useState,
} from "react";
import { useContext } from "react";

type TShowBlock = {
  isShow: true | false;
  setIsShow: Dispatch<SetStateAction<boolean>>;
};

export const Context = createContext({
  isShow: false,
} as TShowBlock);

export const Provider: React.FC = ({ children }) => {
  const [isShow, setIsShow] = useState<boolean>(false);
  return (
    <Context.Provider
      value={{
        isShow: isShow,
        setIsShow,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useContextBlock = () => useContext(Context);
