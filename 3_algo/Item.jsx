import React from "react";
import data from "./data.json";

export const Item = () => {
  return (
    <div>
      {data.map((item, index) => (
        <Item key={index} newValue={item} />
      ))}
    </div>
  );
};
